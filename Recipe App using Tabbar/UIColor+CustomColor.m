//
//  UIColor+CustomColor.m
//  Recipe App using Tabbar
//
//  Created by Alex Brown on 6/5/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "UIColor+CustomColor.h"

@implementation UIColor (CustomColor)

+(UIColor*)customGreenColor {
    return [UIColor colorWithRed:0 green:1 blue:0 alpha:1];
}

+(UIColor*)customBlueColor {
    return [UIColor colorWithRed:0 green:0 blue:1 alpha:1];
}

+(UIColor*)customWhiteColor {
    return [UIColor colorWithRed:1 green:1 blue:1 alpha:2];
}

+(UIColor*)customPurpleColor {
    return [UIColor colorWithRed:1 green:0 blue:5 alpha:5];
}

+(UIColor*)customOrangeColor {
    return [UIColor colorWithRed: 2 green:0 blue:0 alpha:2];
}


@end
