//
//  DescriptionViewController.h
//  Recipe App using Tabbar
//
//  Created by Alex Brown on 6/4/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DescriptionViewController : UIViewController

@property (nonatomic, retain)
NSDictionary *recipeInfo;
@property (nonatomic, retain)
NSDictionary *dict;

@end
