//
//  Recipe.h
//  Recipe App using Tabbar
//
//  Created by Alex Brown on 6/5/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Recipe : NSObject

@property (nonatomic, strong) NSString *recipeTitle;
@property (nonatomic, strong) NSString *recipeIstructions;
@property (nonatomic, strong) NSString *recipeImage; 


@end
