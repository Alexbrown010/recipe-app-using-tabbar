//
//  CustomLabel.m
//  Recipe App using Tabbar
//
//  Created by Alex Brown on 6/5/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "CustomLabel.h"

@implementation CustomLabel

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor customWhiteColor];
        self.font = [UIFont customFont1];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
