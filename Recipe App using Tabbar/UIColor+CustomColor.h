//
//  UIColor+CustomColor.h
//  Recipe App using Tabbar
//
//  Created by Alex Brown on 6/5/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CustomColor)

+(UIColor*)customGreenColor;

+(UIColor*)customBlueColor;

+(UIColor*)customWhiteColor;

+(UIColor*)customPurpleColor;

+(UIColor*)customOrangeColor;

@end
