//
//  ViewController.m
//  Recipe App using Tabbar
//
//  Created by Alex Brown on 6/4/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    TitleViewController * tvc = [TitleViewController new];
    DescriptionViewController * dvc = [DescriptionViewController new];
    
    NSArray *tabViewControllers = @[tvc, dvc];
    
    [self setViewControllers:tabViewControllers];
    
    tvc.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"Recipes" image:nil tag:0];
    
    dvc.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"Recipe Descriptions" image:nil tag:1];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
