//
//  TitleViewController.m
//  Recipe App using Tabbar
//
//  Created by Alex Brown on 6/4/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "TitleViewController.h"

@interface TitleViewController ()

@end

@implementation TitleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Custom background color for this viewcontroller
    self.view.backgroundColor = [UIColor customWhiteColor];
    
    //PList containing recipe info
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Property List" ofType:@"plist"];
    dictOfInfo = [[NSDictionary alloc]initWithContentsOfFile:filePath];
    
    //Crepe recipe button
    UIButton *btnCrepe = [UIButton buttonWithType:UIButtonTypeSystem];
    [btnCrepe setTitle:@"Yummy Crepěs" forState:UIControlStateNormal];
    [btnCrepe addTarget:self action:@selector(btnTouched1:) forControlEvents:UIControlEventTouchUpInside];
     btnCrepe.frame = CGRectMake(0, 60, self.view.frame.size.width, 30);
    [self.view addSubview:btnCrepe];
    
    //French Toast recipe button
    UIButton *btnFT = [UIButton buttonWithType:UIButtonTypeSystem];
    [btnFT setTitle:@"French Toast" forState:UIControlStateNormal];
    [btnCrepe addTarget:self action:@selector(btnTouched1:) forControlEvents:UIControlEventTouchUpInside];
    btnFT.frame = CGRectMake(0, 110, self.view.frame.size.width, 30);
    [self.view addSubview:btnFT];
    
    //Oatmeal recipe button
    UIButton *btnOatmeal = [UIButton buttonWithType:UIButtonTypeSystem];
    [btnOatmeal setTitle:@"Oatmeal" forState:UIControlStateNormal];
    [btnOatmeal addTarget:self action:@selector(btnTouched1:) forControlEvents:UIControlEventTouchUpInside];
    btnOatmeal.frame = CGRectMake(0, 170, self.view.frame.size.width, 30);
    [self.view addSubview:btnOatmeal];
    
    //Omelette recipe button
    UIButton *btnOmelette = [UIButton buttonWithType:UIButtonTypeSystem];
    [btnOmelette setTitle:@"Omelette" forState:UIControlStateNormal];
    [btnOmelette addTarget:self action:@selector(btnTouched1:) forControlEvents:UIControlEventTouchUpInside];
    btnOmelette.frame = CGRectMake(0, 230, self.view.frame.size.width, 30);
    [self.view addSubview:btnOmelette];
    
    //Waffles recipe button
    UIButton *btnWaffles = [UIButton buttonWithType:UIButtonTypeSystem];
    [btnWaffles setTitle:@"Waffles" forState:UIControlStateNormal];
    [btnWaffles addTarget:self action:@selector(btnTouched1:) forControlEvents:UIControlEventTouchUpInside];
    btnWaffles.frame = CGRectMake(0, 290, self.view.frame.size.width, 30);
    [self.view addSubview:btnWaffles];
}
    //Button touched actions
-(void)btnTouched1:(id)sender {
    //Send the dictionary through the notification center 
    NSDictionary *recipeInfo;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"recipeDict" object:recipeInfo];
//        NSLog(@"%@", recipeInfo);
     
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
