//
//  UIFont+CustomFonts.m
//  Recipe App using Tabbar
//
//  Created by Alex Brown on 6/5/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "UIFont+CustomFonts.h"

@implementation UIFont (CustomFonts)

+(UIFont*)customFont1 {
    return [UIFont fontWithName:@"Snell Roundhand" size:25];
}

+(UIFont*)customFont2 {
    return [UIFont fontWithName:@"Courier-Bold" size:25];
}

@end
