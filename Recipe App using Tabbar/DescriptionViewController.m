//
//  DescriptionViewController.m
//  Recipe App using Tabbar
//
//  Created by Alex Brown on 6/4/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "DescriptionViewController.h"

@interface DescriptionViewController ()

@end

@implementation DescriptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Custom background color for this viewcontroller
    self.view.backgroundColor = [UIColor customPurpleColor];
    
    //NSNotification reciever
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(recieved:) name:@"recipeDict" object:nil];

    //Recipe Title Label
    
    CustomLabel* lblTitle = [[CustomLabel alloc]initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 30)];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.text = [self.dict objectForKey:@"Recipe"];
    [self.view addSubview:lblTitle];
    
    //Recipe image
    
    UIImageView* iv = [[UIImageView alloc]initWithFrame:CGRectMake(0, 130, self.view.frame.size.width, 300)];
    iv.contentMode = UIViewContentModeScaleAspectFit;
    [iv setImage: [UIImage imageNamed: [self.dict objectForKey:@"Image"]]];
    [self.view addSubview:iv];
    
    //Recipe directions label
    
    CustomLabel* directionsTxt = [[CustomLabel alloc]initWithFrame:CGRectMake(0, 180, self.view.frame.size.width,self.view.frame.size.height)];
    directionsTxt.text = [self.dict objectForKey:@"Description"];
    directionsTxt.textAlignment = NSTextAlignmentCenter;
    directionsTxt.lineBreakMode = NSLineBreakByWordWrapping;
    directionsTxt.numberOfLines = 0;
    [self.view addSubview:directionsTxt];

}
    //NSNotification function
- (void)recieved:(NSNotification*)n {
        NSDictionary *dict = n.object;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
